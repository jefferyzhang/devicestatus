#include "stdafx.h"
#include "devicestatus.h"
extern TPARAMETERS g_args;

VOID
Oops(
	__in PCTSTR File,
	ULONG Line,
	DWORD dwError)
{
	EnterCriticalSection(&g_args.CriticalSection);
	TCHAR szBuf[2048] = { 0 };

	_stprintf_s(szBuf, _T("File: %s, Line %d, Error %d\n"), File, Line, dwError);
	OutputDebugString(szBuf);
	LeaveCriticalSection(&g_args.CriticalSection);
}

void _logIt(TCHAR* fmt, ...)
{
	EnterCriticalSection(&g_args.CriticalSection);
	va_list args;
	va_start(args, fmt);
	TCHAR s[2048] = { 0 };
	_vstprintf_s(s, fmt, args);
	OutputDebugString(s);
	LeaveCriticalSection(&g_args.CriticalSection);
}
void logIt(TCHAR* fmt, ...)
{
	EnterCriticalSection(&g_args.CriticalSection);
	va_list args;
	va_start(args, fmt);
	TCHAR s[2048] = {0};
	_vstprintf_s(s, fmt, args);
	//_tprintf(s); _tprintf(_T("\n"));
	if (g_args.label > 0)
	{
		TCHAR ss[2048] = {0};
		_stprintf_s(ss, _T("[%04d] %s\n"), g_args.label, s);
		OutputDebugString(ss);
	}
	else
	{
		OutputDebugString(s);
	}
	va_end(args);
	LeaveCriticalSection(&g_args.CriticalSection);
}

void logHex(BYTE* buffer, size_t buf_sz)
{
	TCHAR line_buf[75];
	size_t pos=0;
	int line_count = 0;
	while (pos<buf_sz)
	{
		int line_pos_1 = 0;
		int line_pos_2 = 0;
		ZeroMemory(line_buf, sizeof(line_buf));
		_stprintf_s(line_buf, _T("%04d:"), line_count);
		line_buf[5] = ' ';
		line_pos_1 = 6;
		line_pos_2 = 54;
		for (size_t i = 0; i < 16 && pos<buf_sz; i++, pos++)
		{
			_stprintf_s(&line_buf[line_pos_1], 3, _T("%02x"), buffer[pos]);
			line_pos_1 += 2;
			line_buf[line_pos_1] = ' ';
			line_pos_1++;
			if (isprint(buffer[pos]))
				line_buf[line_pos_2] = (char)buffer[pos];
			else
				line_buf[line_pos_2] = '.';
			line_pos_2++;
		}
		for (; line_pos_1 < 54; line_pos_1++)
		{
			line_buf[line_pos_1] = ' ';
		}
		line_buf[line_pos_2] = 0;
		logIt(_T("%s\n"), line_buf);
		line_count++;
	}
}