#include "stdafx.h"
#include "devicestatus.h"

using namespace std;

map<wstring, wstring> g_argMap;
//int  CheckHwid(int nlabel, LPCTSTR sHwid, LPCTSTR sInfname);

void addOrModifyArgMapByKeyAndValue(wchar_t* key, wchar_t* value)
{
	if (key != NULL && wcslen(key)>0)
	{
		wstring v;
		if (value == NULL) v = L"";
		else v = value;
		auto r = g_argMap.insert(make_pair(key, v));
		if (!r.second)
		{
			// key already exists
			g_argMap[key] = v;
		}
	}
}
void parseCommandLine(LPSTR lpszCmdLine)
{
	LPWSTR *szArglist;
	int nArgs;
	wchar_t cmdLine[1024 * 4];
	errno_t err = mbstowcs_s((size_t*)&nArgs, cmdLine, 1000, lpszCmdLine, 1000);
	if (err == NO_ERROR)
	{
		szArglist = CommandLineToArgvW(cmdLine, &nArgs);
		if (szArglist != NULL)
		{
			for (int i = 0; i < nArgs; i++)
			{
				if (szArglist[i][0] == '-')
				{
					wchar_t *key = &szArglist[i][1];
					wchar_t *value = wcschr(key, '=');
					if (value != NULL)
					{
						*value = '\0';
						value++;
					}
					addOrModifyArgMapByKeyAndValue(key, value);
				}
			}
			LocalFree(szArglist);
		}
	}
}

void test_CheckHwid()
{
	int err = NO_ERROR;
	auto hwid = g_argMap.find(L"hwid");
	auto inf = g_argMap.find(L"inf");
	if (hwid != g_argMap.end() && inf != g_argMap.end())
	{
		checkHwidExist(0, hwid->second.c_str(), inf->second.c_str());
	}
}
void test_GetDevicePropertiesByDeviceInstanceIdEx()
{
	//struct _DEVICEPROPERTY_T
	//{
	//	DEVPROPKEY key;
	//	DEVPROPTYPE type;
	//	DWORD buflen;
	//	BYTE buf[1024];
	//} dp[3];
	//struct _DEVICEPROPERTY_T * dps = new _DEVICEPROPERTY_T[3];

	//_DeviceProperty *dp = (_DeviceProperty*) new _DeviceProperty[3];
	_DeviceProperty dp[3];
	// prepare
	memcpy((BYTE*)&dp[0].key, (BYTE*)&DEVPKEY_Device_DeviceDesc, sizeof(DEVPROPKEY));
	memcpy((BYTE*)&dp[1].key, (BYTE*)&DEVPKEY_Device_Service, sizeof(DEVPROPKEY));
	memcpy((BYTE*)&dp[2].key, (BYTE*)&DEVPKEY_Device_Driver, sizeof(DEVPROPKEY));
	
	auto hwid = g_argMap.find(L"hwid");
	if (hwid != g_argMap.end())
	{
		//DWORD dps[3];
		//dps[0] = (DWORD)&dp[0];
		//dps[1] = (DWORD)&dp[1];
		//dps[2] = (DWORD)&dp[2];
		_DeviceProperty* pdp = &dp[0];
		//int ret = GetDevicePropertiesByDeviceInstanceIdEx(0, hwid->second.c_str(), &pdp, 3);
		TCHAR lpHubName[1024] = { 0 };
		DWORD nSizePort = 1024;//in lpHubName Buffer Length, Out is Hub port

		int ret = GetDevicePropertiesByDeviceSerialNumberEx(0, hwid->second.c_str(), lpHubName,
			nSizePort,//in lpHubName Buffer Length, Out is Hub port
			&pdp, 3);
	}
}

void CALLBACK test(HWND hwnd, HINSTANCE hinst, LPSTR lpszCmdLine, int nCmdShow)
{
	parseCommandLine(lpszCmdLine);

	auto debug = g_argMap.find(L"debug");
	if (debug != g_argMap.end())
	{
		MessageBox(NULL, _T("Wait for debugger"), _T("Press ok to continue"), MB_OK);
	}

	//test_CheckHwid();
	test_GetDevicePropertiesByDeviceInstanceIdEx();
}