#ifndef _EXE__LIBRARY__H_
#define _EXE__LIBRARY__H_

#ifdef EXE_LIBRARY
#define EXE_LIBRARY_API __declspec(dllexport)
#else
#define EXE_LIBRARY_API __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C" {  // only need to export C interface if
#endif

	EXE_LIBRARY_API int  GetDeviceStatusHW(int nlabel, LPCTSTR sHwid);
	EXE_LIBRARY_API int  GetDeviceStatusSymbl(int nlabel, LPCTSTR symbl, int nPort);
	EXE_LIBRARY_API int  GetDeviceLabelPort(int nlabel, LPCTSTR sHwid, LPCTSTR sFileName);
	EXE_LIBRARY_API int  dllmain(int argc, _TCHAR* argv[]);
	EXE_LIBRARY_API int  GetSemaphoreCurCnt(LPTSTR sSemaphoreName);
	EXE_LIBRARY_API int  GetDeviceInstanceIdFromSymbl(
		_In_	 int nlabel,
		_In_     LPCWSTR symbl,
		_In_	 int nPort,
		_Out_writes_to_opt_(nSize, return +1) LPWSTR lpReturnedString,
		_In_     DWORD nSize
		);
	EXE_LIBRARY_API int  GetHubPortIndex(int nlabel, LPCTSTR sHubName);
	EXE_LIBRARY_API int  GetCommonSymbl(int nlabel, LPCTSTR sHubName, int nPort, PDEVPROPKEY pdevkey, PBYTE nRet, int &nLen, DWORD &nType);
	EXE_LIBRARY_API int  GetCommonHW(int nlabel, LPCTSTR sHwid, PDEVPROPKEY pdevkey, PBYTE nRet, int &nLen, DWORD &nType);
	EXE_LIBRARY_API int  CheckHwid(int nlabel, LPCTSTR sHwid, LPCTSTR sInfname);
	EXE_LIBRARY_API int  GetDevicePropertiesByDeviceInstanceId(int nlabel, LPCTSTR sHwid, LPVOID pdata, int dataCount);
	//this has problem, it is deprecated 
	EXE_LIBRARY_API int  GetDeviceSymblFromInstanceId(
		_In_	 int nlabel,
		_In_     LPCWSTR sInstanceId,
		_Out_writes_to_opt_(nSize, return +1) LPWSTR lpReturnedString,
		_In_     DWORD nSize
		);
	EXE_LIBRARY_API int  GetDeviceSymblFromInstanceIdEx(
		_In_	 int nlabel,
		_In_     LPCWSTR sInstanceId,
		_Out_writes_to_opt_(nSize, return +1) LPWSTR lpReturnedString,
		_In_     DWORD nSize,
		_In_     GUID  interfaceguid
		);
	EXE_LIBRARY_API int  GetChildPropertyFromHubPort(
		_In_	 int nlabel,
		_In_     LPCWSTR sHubName,
		_In_	 int nPort,
		_In_	 PDEVPROPKEY pdevkey, 
		_Out_    PBYTE nRet, 
		_Inout_  int &nLen, 
		_Out_    DWORD &nType
		);
	EXE_LIBRARY_API int  GetDevicePropertiesByDeviceSerialNumber(
		_In_	 int nlabel,
		_In_	 LPCTSTR sSerialNumber,
		_Out_writes_to_opt_(nSize, return +1) LPWSTR lpHubName,
		_Inout_  DWORD &nSizePort,//in lpHubName Buffer Length, Out is Hub port
		_Inout_	 LPVOID pdata,
		_In_	 int dataCount
		);
#ifdef __cplusplus
}
#endif

#endif