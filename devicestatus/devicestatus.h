#pragma once
#include "stdafx.h"

typedef struct __tagPARAMETERS_T
{
	__tagPARAMETERS_T()
	{
		label = 0;
		nPort = 0;
		InitializeCriticalSection(&CriticalSection);
		InitializeCriticalSection(&csLockglobe);
	}
	~__tagPARAMETERS_T()
	{
		DeleteCriticalSection(&CriticalSection);
		DeleteCriticalSection(&csLockglobe);
	}
	int		label;
	int		nPort;
	STRING  sName;
	STRING  sHWid;
	STRING	sCalibration;
	STRING	sPropKey;	//GUID\type
	CRITICAL_SECTION CriticalSection;
	CRITICAL_SECTION csLockglobe;
}TPARAMETERS, *PTPARAMETERS;


typedef struct _tagDEVICEPROPERTY_T
{
	DEVPROPKEY key;
	DEVPROPTYPE type;
	DWORD buflen;
	BYTE buf[1024];
} _DeviceProperty;

// from logit.cpp
void _logIt(TCHAR* fmt, ...);
void logIt(TCHAR* fmt, ...);
void logHex(BYTE* buffer, size_t buf_sz);
void usage();
int parseCommandLine(int argc, _TCHAR* argv[], TPARAMETERS &_args);

// from SetupApi.cpp
//int GetDevicePropertiesByDeviceInstanceIdEx(int nlabel, LPCTSTR sHwid, LPVOID pdata, int dataCount);
int GetDevicePropertiesByDeviceInstanceIdEx(int nlabel, LPCTSTR sHwid, _DeviceProperty** pDeviceProps, int dataCount);
int  GetDevicePropertiesByDeviceSerialNumberEx(int nlabel, LPCTSTR sSerialNumber, _Out_writes_to_opt_(nSize, return +1) LPWSTR lpHubName,
	_Inout_  DWORD &nSizePort,//in lpHubName Buffer Length, Out is Hub port
	_DeviceProperty** pDeviceProps, int dataCount);

VOID
Oops
(
	__in PCTSTR File,
	ULONG Line,
	DWORD dwError
);

#define OOPS()		Oops(_T(__FILE__), __LINE__, GetLastError())
#define OOPSERR(d)	Oops(_T(__FILE__), __LINE__, d)
#define ENTER_FUNCTION()	logIt(_T("%s ++\n"), _T(__FUNCTION__))
#define EXIT_FUNCTION()		logIt(_T("%s --\n"), _T(__FUNCTION__))
#define EXIT_FUNCTRET(ret)		logIt(_T("%s -- return=%d\n"), _T(__FUNCTION__), ret)

// from checkHwid.cpp
BOOL checkHwidExist(int nlabel, LPCTSTR sHwid, LPCTSTR sInfname);