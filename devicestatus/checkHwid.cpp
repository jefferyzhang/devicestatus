#include "stdafx.h"
#include "devicestatus.h"

BOOL check64BitOS()
{
	BOOL ret = FALSE;
	if (IsWow64Process(GetCurrentProcess(), &ret))
	{
		// failed?
		logIt(_T("check64BitOS: get error %d\n"), GetLastError());
	}
	return ret;
}
BOOL foundHardwareIdBySection(HINF hInf, TCHAR* sectionName, TCHAR* diid)
{
	BOOL ret = FALSE;
	TCHAR buf[1024];
	if (check64BitOS())
	{
		// 64bit os
		_stprintf_s(buf, 1000,_T("%s.ntamd64"), sectionName);
	}
	else
	{
		// 32bit os
		_stprintf_s(buf, 1000, _T("%s.ntx86"), sectionName);
		if (SetupGetLineCount(hInf, buf) > 0){}
		else _stprintf_s(buf, 1000, _T("%s"), sectionName);
	}
	logIt(_T("foundHardwareIdBySection: ++ %s, %s\n"), buf, diid);
	LONG line_count = SetupGetLineCount(hInf, buf);
	for (size_t line = 0; line < line_count && line_count > 0 && !ret; line++)
	{
		INFCONTEXT context;
		if (SetupGetLineByIndex(hInf, buf, line, &context))
		{
			DWORD field_count = SetupGetFieldCount(&context);
			if (field_count >= 2)
			{
				TCHAR *hwid = &buf[wcslen(buf) + 16];
				if (SetupGetStringField(&context, 2, hwid, MAX_PATH, NULL))
				{
					logIt(_T("%s vs. %s\n"), hwid, diid);
					if (_tcsclen(hwid) > 0 && _tcsnicmp(hwid, diid, _tcsclen(hwid)) == 0 &&
						(diid[wcslen(hwid)] == '\\' || diid[wcslen(hwid)] == '\0'))
					{
						ret = TRUE;
						logIt(_T("found %s.\n"), hwid);
					}
				}
			}
		}
	}
	logIt(_T("foundHardwareIdBySection: -- %d\n"), ret);
	return ret;
}
BOOL checkHwidExist(int nlabel, LPCTSTR sHwid, LPCTSTR sInfname)
{
	BOOL ret = FALSE;
	_logIt(_T("[label_%d] checkHwidExist: ++ hwid=%s, inf=%s\n"), nlabel, sHwid, sInfname);
	HINF hInf = SetupOpenInfFile(sInfname, NULL, INF_STYLE_WIN4, NULL);
	if (hInf != INVALID_HANDLE_VALUE)
	{
		LONG line_count = SetupGetLineCount(hInf, _T("Manufacturer"));
		for (size_t line = 0; line < line_count && line_count > 0 && !ret; line++)
		{
			INFCONTEXT context;
			if (SetupGetLineByIndex(hInf, _T("Manufacturer"), line, &context))
			{
				if (SetupGetFieldCount(&context) >= 1)
				{
					TCHAR deviceSection[MAX_PATH];
					if (SetupGetStringField(&context, 1, deviceSection, MAX_PATH, NULL))
					{
						ret = foundHardwareIdBySection(hInf, deviceSection, (TCHAR*)sHwid);
					}
					else
						OOPS();
				}
				else
					OOPS();
			}
			else
				OOPS();
		}
		SetupCloseInfFile(hInf);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}
	logIt(_T("[label_%d] checkHwidExist: -- ret=%d\n"), nlabel, ret);
	return ret;
}