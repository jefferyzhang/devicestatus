#include "stdafx.h"
#include "devicestatus.h"

extern TPARAMETERS g_args;
void usage()
{
	_tprintf(_T("Command line:\n"));
	_tprintf(_T("\t -label=1 -port=2 -hubname=USB#***{f18a0e88-c30c-11d0-8815-00a0c906bed8}"));
	_tprintf(_T("\t\t input hub info. get connect this port device status."));
	_tprintf(_T("\t -label=1 -hwid=USB\\***\\BA04DC85"));
	_tprintf(_T("\t\t input usb devices. get this devices status."));
	_tprintf(_T("return USB Device driver status."));
}

int parseCommandLine(int argc, _TCHAR* argv[], TPARAMETERS &_args)
{
	int ret = NO_ERROR;
	for (int i = 0; i < argc; i++)
	{
		logIt(_T("%s\n"), argv[i]);
		if (argv[i] != NULL && (argv[i][0] == _T('-') || argv[i][0] == _T('/')))
		{
			TCHAR* v = _tcschr(argv[i], _T('='));
			if (v != NULL) v++;
			if (_tcsncicmp(&argv[i][1], _T("label"), 5) == 0)
			{
				if (v != NULL)
					_args.label = _tstoi(v);
			}
			else if (_tcsncicmp(&argv[i][1], _T("port"), 4) == 0)
			{
				if (v != NULL)
					_args.nPort = _tstoi(v);
			}
			else if (_tcsncicmp(&argv[i][1], _T("hubname"), 7) == 0)
			{
				if (v != NULL)
				{
					_args.sName = v;
				}
			}
			else if (_tcsncicmp(&argv[i][1], _T("hwid"), 4) == 0)
			{
				if (v != NULL)
					_args.sHWid = v;
			}
			else if (_tcsncicmp(&argv[i][1], _T("calibration"), 11) == 0)
			{
				if (v != NULL)
					_args.sCalibration = v;
			}
			else if (_tcsncicmp(&argv[i][1], _T("propkey"), 7) == 0)
			{
				if (v != NULL)
					_args.sPropKey = v;
			}
		}
	}
	return ret;
}
