// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <Windows.h>

#include <map>
#include <string>
#include <vector>

#include <Shlwapi.h>
#define INITGUID
#include <initguid.h>
#include <SetupAPI.h>
#include <Devpkey.h>
#include <Devpropdef.h>

#include <Cfg.h>
#include <Usbiodef.h>
#include <Cfgmgr32.h>

#include <usbioctl.h>

// TODO: reference additional headers your program requires here
#ifdef _UNICODE
#define STRING	std::wstring
#else
#define STRING	std::string
#endif

#pragma comment(lib, "Shlwapi.lib")
#pragma comment(lib, "setupapi.lib")
#pragma comment(lib, "Cfgmgr32.lib")

