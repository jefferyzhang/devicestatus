#include "stdafx.h"
#include "devicestatus.h"
#include <Usbiodef.h>  

bool    GetStringDescriptor(
	HANDLE hHubDevice,
	ULONG   ConnectionIndex,
	UCHAR   DescriptorIndex,
	USHORT  LanguageID,
	TCHAR * outBuff
	)
{
	BOOL    success;
	ULONG   nBytes;
	ULONG   nBytesReturned;

	TCHAR   stringDescReqBuf[sizeof(USB_DESCRIPTOR_REQUEST) + MAXIMUM_USB_STRING_LENGTH];

	PUSB_DESCRIPTOR_REQUEST stringDescReq;
	PUSB_STRING_DESCRIPTOR stringDesc;

	nBytes = sizeof(stringDescReqBuf);

	stringDescReq = (PUSB_DESCRIPTOR_REQUEST)stringDescReqBuf;
	stringDesc = (PUSB_STRING_DESCRIPTOR)(stringDescReq + 1);


	::ZeroMemory(stringDescReq, nBytes);
	stringDescReq->ConnectionIndex = ConnectionIndex;
	stringDescReq->SetupPacket.wValue = (USB_STRING_DESCRIPTOR_TYPE << 8) | DescriptorIndex;
	stringDescReq->SetupPacket.wIndex = LanguageID;//GetSystemDefaultLangID();
	stringDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

	success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION,
		stringDescReq, nBytes,
		stringDescReq, nBytes,
		&nBytesReturned, NULL);

	if (!success || nBytesReturned < 2)
		return false;

	if (stringDesc->bDescriptorType != USB_STRING_DESCRIPTOR_TYPE)
		return false;

	if (stringDesc->bLength != nBytesReturned - sizeof(USB_DESCRIPTOR_REQUEST))
		return false;

	if (stringDesc->bLength % 2 != 0)
		return false;

	lstrcpy(outBuff, stringDesc->bString);

	return true;
}

int  GetDevicePropertiesByDeviceSerialNumberEx(int nlabel, LPCTSTR sSerialNumber, _Out_writes_to_opt_(nSize, return +1) LPWSTR lpHubName,
	_Inout_  DWORD &nSizePort,//in lpHubName Buffer Length, Out is Hub port
	_DeviceProperty** pDeviceProps, int dataCount)
{
	int ret = ERROR_INVALID_PARAMETER;
	_logIt(_T("[label_%d] GetDevicePropertiesByDeviceInstanceIdEx: ++ hwid=%s, proterty count=%d\n"), nlabel, sSerialNumber, dataCount);
	_DeviceProperty* ppDp = (pDeviceProps != NULL) ? *pDeviceProps : NULL;
	if (ppDp != NULL && sSerialNumber != NULL && _tcslen(sSerialNumber) > 0)
	{
		//usb device SerailNumber
		LPGUID lpGuid = (LPGUID)&GUID_DEVINTERFACE_USB_HUB;//  
		int INTERFACE_DETAIL_SIZE = 1024;
		int nCount = 0;
		BOOL bFind = FALSE;
		HDEVINFO info = SetupDiGetClassDevs(lpGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);
		if (info!=NULL)
		{
			SP_DEVICE_INTERFACE_DATA ifdata = { sizeof(SP_DEVICE_INTERFACE_DATA) };
			SP_DEVINFO_DATA DeviceInfoData = { sizeof(DeviceInfoData) };
			DWORD devindex;
			for (devindex = 0; SetupDiEnumDeviceInterfaces(info, NULL, lpGuid, devindex, &ifdata) && !bFind; ++devindex)
			{	
				TCHAR  DetailDataBuffer[1024] = { 0 };
				PSP_DEVICE_INTERFACE_DETAIL_DATA DetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)DetailDataBuffer;
				DetailData->cbSize = sizeof SP_INTERFACE_DEVICE_DETAIL_DATA;
				int needed = 1024;

				if (SetupDiGetDeviceInterfaceDetail(info, &ifdata, DetailData, needed, NULL, &DeviceInfoData))
				{						// can't get detail info
					SECURITY_ATTRIBUTES SA = { 0 };
					SA.nLength = sizeof(SECURITY_ATTRIBUTES);
					HANDLE HubHandle = CreateFile(DetailData->DevicePath, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_WRITE | FILE_SHARE_READ, &SA, OPEN_EXISTING, 0, NULL);
					if (HubHandle != INVALID_HANDLE_VALUE)
					{
						DWORD BytesReturned = 0;
						USB_NODE_INFORMATION HubInfo;
						BOOL success = DeviceIoControl(HubHandle,
							IOCTL_USB_GET_NODE_INFORMATION,
							&HubInfo, sizeof(USB_NODE_INFORMATION),
							&HubInfo, sizeof(USB_NODE_INFORMATION),
							&BytesReturned,
							NULL);
						if (success){
							for (int index = 1; index <= HubInfo.u.HubInformation.HubDescriptor.bNumberOfPorts && !bFind; index++)
							{
								PUSB_NODE_CONNECTION_INFORMATION  connectionInfo;
								ULONG nBytes = sizeof(USB_NODE_CONNECTION_INFORMATION) +
									sizeof(USB_PIPE_INFO) * 30;
								connectionInfo = (PUSB_NODE_CONNECTION_INFORMATION)malloc(nBytes);
								if (connectionInfo == NULL)
								{
									continue;
								}
								connectionInfo->ConnectionIndex = index;
								BOOL Success = DeviceIoControl(HubHandle, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION,
									connectionInfo, nBytes, connectionInfo, nBytes, &BytesReturned, NULL);
								if (Success && !connectionInfo->DeviceIsHub && connectionInfo->ConnectionStatus == DeviceConnected)
								{
									UCHAR nSerialno = connectionInfo->DeviceDescriptor.iSerialNumber;
									TCHAR OutBuff[MAXIMUM_USB_STRING_LENGTH] = { 0 };
									GetStringDescriptor(HubHandle, connectionInfo->ConnectionIndex, 0, 0, OutBuff);
									//Hard code set languageID
									USHORT langid = OutBuff[0] == 0 ? 0x0409 : OutBuff[0];
									if (GetStringDescriptor(HubHandle, connectionInfo->ConnectionIndex, nSerialno, langid, OutBuff))
									{
										if (_tcscmp(OutBuff, sSerialNumber)==0)
										{
											//memcpy(lpHubName, DetailData->DevicePath, (_tcslen(DetailData->DevicePath) + 1)*sizeof(TCHAR));
											_stprintf_s(lpHubName, nSizePort, _T("%s"), DetailData->DevicePath);
											nSizePort = index;
											//find Device
											DEVPROPTYPE type;
											BYTE b[2048] = { 0 };
											ZeroMemory(b, sizeof(b));
											DWORD sz = 0;

											for (size_t i = 0; i < dataCount; i++, ppDp++)
											{
												_DeviceProperty* dp = ppDp;
												ZeroMemory(b, sizeof(b));
												sz = 0;
												if (SetupDiGetDeviceProperty(info, &DeviceInfoData, &(dp->key), &type, b, sizeof(b), &sz, 0))
												{
													dp->type = type;
													dp->buflen = sz;
													memcpy(&dp->buf, b, sz);
												}
											}
											ret = NOERROR;
											bFind = TRUE;
											free(connectionInfo);
											break;
										}
									}
								}
								free(connectionInfo);
							}
						}
						else
						{
							ret = GetLastError();
							OOPSERR(ret);
						}
						CloseHandle(HubHandle);
					}
					else{
						ret = GetLastError();
						OOPSERR(ret);
					}
				}
				else// can't get detail info
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}
		if (!bFind && ret == ERROR_SUCCESS)
		{
			ret = ERROR_NOT_FOUND;
		}
	}
	else
	{
		OOPSERR(ret);
	}
	logIt(_T("[label_%d] GetDevicePropertiesByDeviceInstanceIdEx: -- ret=%d\n"), nlabel, ret);
	return ret;

}

int GetDevicePropertiesByDeviceInstanceIdEx(int nlabel, LPCTSTR sHwid, _DeviceProperty** pDeviceProps, int dataCount)
{
	int ret = ERROR_INVALID_PARAMETER;
	_logIt(_T("[label_%d] GetDevicePropertiesByDeviceInstanceIdEx: ++ hwid=%s, proterty count=%d\n"), nlabel, sHwid, dataCount);
	_DeviceProperty* ppDp = (pDeviceProps != NULL) ? *pDeviceProps : NULL;
	if (ppDp != NULL && sHwid != NULL && _tcslen(sHwid) > 0)
	{
		HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
		if (hDevInfo != INVALID_HANDLE_VALUE)
		{
			SP_DEVINFO_DATA devInfoData;
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			if (SetupDiOpenDeviceInfo(hDevInfo, sHwid, NULL, 0, &devInfoData))
			{
				DEVPROPTYPE type;
				BYTE b[2048];
				ZeroMemory(b, sizeof(b));
				DWORD sz = 0;
				for (size_t i = 0; i < dataCount; i++, ppDp++)
				{
					_DeviceProperty* dp = ppDp;
					ZeroMemory(b, sizeof(b));
					sz = 0;
					if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &(dp->key), &type, b, sizeof(b), &sz, 0))
					{
						dp->type = type;
						dp->buflen = sz;
						memcpy(&dp->buf, b, sz);
					}
				}
				ret = NOERROR;
			}
			else 
			{
				OOPS();
			}
			SetupDiDestroyDeviceInfoList(hDevInfo);
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}
	}
	else
		OOPSERR(ret);
	logIt(_T("[label_%d] GetDevicePropertiesByDeviceInstanceIdEx: -- ret=%d\n"), nlabel,ret);
	return ret;
}