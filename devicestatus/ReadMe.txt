Command line:
	 -label=1 -port=2 -hubname=USB#***{f18a0e88-c30c-11d0-8815-00a0c906bed8}
		 input hub info. get connect this port device status.
	 -label=1 -hwid=USB\\***\\BA04DC85
		 input usb devices. get this devices status.
return USB Device driver status.


add selftesting interface.
C:\ProgramData\Futuredial\CMC>rundll32.exe DeviceStatus.dll,test -debug -inf=c:\Windows\inf\oem10.inf -hwid="USB\VID_04E8&PID_6860&adb

use rundll32.exe to execute "test" interface. in the test function, we can test the code internal.

c# code example:
        [StructLayout(LayoutKind.Sequential)]
        public struct DEVPROPKEY
        {
            public Guid fmtid;
            public UInt32 pid;
        }

                
        [StructLayout(LayoutKind.Sequential)]
        public struct DEVPROPDATA
        {
            public DEVPROPKEY key;
            public int type;
            public int length;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
            public byte[] buf;
        }
        [DllImport("DeviceStatus.Dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, SetLastError = true)]
        //public extern static int GetDevicePropertiesByDeviceInstanceId(int nlabel, string sHwid, IntPtr data, int count);
        public extern static int GetDevicePropertiesByDeviceInstanceId(int nlabel, string sHwid,
            [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)]ref DEVPROPDATA[] data, int count);

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
            try
            {
                DEVPROPDATA[] dp = new DEVPROPDATA[3];
                dp[0].key.fmtid = new Guid(0xa45c254e, 0xdf1c, 0x4efd, 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0);
                //dp[0].key.fmtid = new GUID() { Data1 = 0xa45c254e, Data2 = 0xdf1c, Data3 = 0x4efd, Data4 = new byte[]{ 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0 } };
                dp[0].key.pid = 2;
                dp[1].key.fmtid = new Guid(0xa45c254e, 0xdf1c, 0x4efd, 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0);
                //dp[1].key.fmtid = new GUID() { Data1 = 0xa45c254e, Data2 = 0xdf1c, Data3 = 0x4efd, Data4 = new byte[] { 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0 } };
                dp[1].key.pid = 3;
                dp[2].key.fmtid = new Guid(0xa45c254e, 0xdf1c, 0x4efd, 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0);
                //dp[2].key.fmtid = new GUID() { Data1 = 0xa45c254e, Data2 = 0xdf1c, Data3 = 0x4efd, Data4 = new byte[] { 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0 } };
                dp[2].key.pid = 6;
                int r = GetDevicePropertiesByDeviceInstanceId(0, @"USB\VID_04E8&PID_6860\BA04DC85", ref dp, dp.Length);
                if (r == 0)
                {
                    foreach(DEVPROPDATA d in dp)
                    {
						string s = System.Text.Encoding.Unicode.GetString(d.buf, 0, d.length);
                    }
                }
            }
            catch (Exception) { }
		}

version:1.0.0.8
Get Hub Connect Device Property.
int  GetChildPropertyFromHubPort(
		_In_	 int nlabel,
		_In_     LPCWSTR sHubName,
		_In_	 int nPort,
		_In_	 PDEVPROPKEY pdevkey, 
		_Out_    PBYTE nRet, 
		_Inout_  int &nLen, 
		_Out_    DWORD &nType
		);

version:1.0.0.9
Get Hub name and hubport for USB Serial Number
and can read hub some Porperties. Like GetDevicePropertiesByDeviceInstanceId, but this api read device info
if Mutlidevice have the same Serial Number, return first device 
EXE_LIBRARY_API int  GetDevicePropertiesByDeviceSerialNumber(
		_In_	 int nlabel,
		_In_	 LPCTSTR sSerialNumber,
		_Out_writes_to_opt_(nSize, return +1) LPWSTR lpHubName,
		_Inout_  DWORD &nSizePort,//in lpHubName Buffer Length, Out is Hub port
		_Inout_	 LPVOID pdata,
		_In_	 int dataCount
		);
test code :rundll32.exe DeviceStatus.dll test -debug -hwid=f5259a25
