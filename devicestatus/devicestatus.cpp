// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "devicestatus.h"
#define EXE_LIBRARY
#include "exelibheader.h"

TPARAMETERS g_args;

void dump(DEVPROPKEY* key, DEVPROPTYPE type, BYTE* buffer, size_t buf_sz)
{
	if (key != NULL)
	{
		logIt(_T("Dump DEVPROPKEY:\n"));
		logIt(_T(" fmtid = {%08lX-%04hX-%04hX-%02hhX%02hhX-%02hhX%02hhX%02hhX%02hhX%02hhX%02hhX}\n"), 
			key->fmtid.Data1, key->fmtid.Data2, key->fmtid.Data3,
			key->fmtid.Data4[0], key->fmtid.Data4[1], key->fmtid.Data4[2], key->fmtid.Data4[3],
			key->fmtid.Data4[4], key->fmtid.Data4[5], key->fmtid.Data4[6], key->fmtid.Data4[7]);
		logIt(_T(" pid = %d\n"), key->pid);
		logIt(_T(" type = %d\n"), type);
	}
	if (buffer != NULL)
	{
		logHex(buffer, buf_sz);
	}
}
///*
int GetDeviceStatus_Test(TCHAR *device_iid)
{
	int ret = NO_ERROR;
	//TCHAR device_iid[] = _T("USB\\VID_04E8&PID_6860\\BA04DC85");
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE) 
	{
		DWORD sz = 0;
		SP_DEVINFO_DATA devInfoData;
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, device_iid, NULL, 0, &devInfoData))
		{
			SetupDiGetDevicePropertyKeys(hDevInfo, &devInfoData, NULL, NULL, &sz, 0);
			LPVOID buf = malloc(sz*sizeof(DEVPROPKEY));
			DWORD count = sz;
			sz = 0;
			if (SetupDiGetDevicePropertyKeys(hDevInfo, &devInfoData, (DEVPROPKEY*)buf, count, &sz, 0))
			{
				DEVPROPKEY* p = (DEVPROPKEY*)buf;
				for (size_t i = 0; i < count; i++, p++)
				{
					DEVPROPTYPE type;
					BYTE b[1024];
					ZeroMemory(b, sizeof(b));
					sz = 0;
					if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, p, &type, b, 1024, &sz, 0))
					{
						logIt(_T("Property: #%d\n"), i + 1);
						dump(p, type, b, sz);
					}
					ZeroMemory(b, sizeof(b));
				}
			}
			free(buf);
		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
		ret = GetLastError();
	return ret;
}

int test2()
{
	int ret = NO_ERROR;
	TCHAR device_iid[] = _T("\\\\?\\USB#VID_058F&PID_9254#6&236cc9f3&0&2#{f18a0e88-c30c-11d0-8815-00a0c906bed8}");
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, device_iid, 0, &devIntData))
		{
			SetupDiGetDeviceInterfacePropertyKeys(hDevInfo, &devIntData, NULL, NULL, &sz, 0);
			LPVOID buf = malloc(sz*sizeof(DEVPROPKEY));
			DWORD count = sz;
			sz = 0;
			if (SetupDiGetDeviceInterfacePropertyKeys(hDevInfo, &devIntData, (DEVPROPKEY*)buf, count, &sz, 0))
			{
				DEVPROPKEY* p = (DEVPROPKEY*)buf;
				for (size_t i = 0; i < count; i++, p++)
				{
					DEVPROPTYPE type;
					BYTE b[1024];
					ZeroMemory(b, sizeof(b));
					sz = 0;
					if (SetupDiGetDeviceInterfaceProperty(hDevInfo, &devIntData, p, &type, b, 1024, &sz, 0))
					{
						logIt(_T("Property: #%d\n"), i + 1);
						dump(p, type, b, sz);
					}
					ZeroMemory(b, sizeof(b));
				}
			}
			free(buf);

			SP_DEVINFO_DATA devInfoData;
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			{
				SetupDiGetDevicePropertyKeys(hDevInfo, &devInfoData, NULL, NULL, &sz, 0);
				LPVOID buf = malloc(sz*sizeof(DEVPROPKEY));
				DWORD count = sz;
				sz = 0;
				if (SetupDiGetDevicePropertyKeys(hDevInfo, &devInfoData, (DEVPROPKEY*)buf, count, &sz, 0))
				{
					DEVPROPKEY* p = (DEVPROPKEY*)buf;
					for (size_t i = 0; i < count; i++, p++)
					{
						DEVPROPTYPE type;
						BYTE b[1024];
						ZeroMemory(b, sizeof(b));
						sz = 0;
						if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, p, &type, b, 1024, &sz, 0))
						{
							logIt(_T("Property: #%d\n"), i + 1);
							dump(p, type, b, sz);
						}
						ZeroMemory(b, sizeof(b));
					}
				}
				free(buf);
			}

		}
		else
			ret = GetLastError();

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
		ret = GetLastError();
	return ret;
}
//*/

int GetDeviceStatus(TCHAR *device_iid)
{
	int ret = ERROR_INVALID_PARAMETER;
	if (device_iid == NULL || _tcslen(device_iid) == 0)
		return ret;
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		
		SP_DEVINFO_DATA devInfoData;
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, device_iid, NULL, 0, &devInfoData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			DWORD sz = sizeof(b);
			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_ProblemCode, &type, b, sz, &sz, 0))
			{
				//logIt(_T("Property: #%d\n"), i + 1);
				//dump(DEVPKEY_Device_ProblemCode, type, b, sz);
				if (type != DEVPROP_TYPE_UINT32)
				{
					logIt(_T("Error info. get DEVPKEY_Device_ProblemCode;"));
				}
				ret = *((DWORD*)b);
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
			if (ret == ERROR_SUCCESS)
			{
				ZeroMemory(b, sizeof(b));
				sz = sizeof(b);
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_DevNodeStatus, &type, b, sz, &sz, 0))
				{
					//logIt(_T("Property: #%d\n"), i + 1);
					//dump(DEVPKEY_Device_ProblemCode, type, b, sz);
					if (type != DEVPROP_TYPE_UINT32)
					{
						logIt(_T("Error info. get DEVPKEY_Device_ProblemCode;"));
					}
					DWORD nodestatus = *((DWORD*)b);
					if ((nodestatus & (DN_DRIVER_LOADED | DN_STARTED)) == (DN_DRIVER_LOADED | DN_STARTED))
					{
						ret = ERROR_SUCCESS;
					}
					else
					{
						ret = nodestatus;
						OOPSERR(ret);
					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}
	return ret;
}

int GetDeviceDriverkey(TCHAR *device_iid, TCHAR *driverkey, BOOL &bFind)
{
	int ret = ERROR_INVALID_PARAMETER;
	if ((device_iid == NULL || _tcslen(device_iid) == 0) || (driverkey == NULL || _tcslen(driverkey) == 0))
		return ret;
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{

		SP_DEVINFO_DATA devInfoData;
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, device_iid, NULL, 0, &devInfoData))
		{
			DEVPROPTYPE type;
			BYTE b[2048] = { 0 };
			ZeroMemory(b, sizeof(b));
			DWORD sz = sizeof(b);
			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_Driver, &type, b, sz, &sz, 0))
			{
				if (_tcsicmp((TCHAR*)b, driverkey)==0)
				{
					bFind = TRUE;
					logIt(_T("find devices."));
				}
				else
				{
					ret = ERROR_DEVICE_REMOVED;
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}
	return ret;

}

int GetDeviceStatusByPort(TCHAR *device_iid, int nPort, BOOL &bFind)
{
	int ret = ERROR_INVALID_PARAMETER;
	if (device_iid == NULL || _tcslen(device_iid) == 0)
		return ret;
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{

		SP_DEVINFO_DATA devInfoData;
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, device_iid, NULL, 0, &devInfoData))
		{
			DEVPROPTYPE type;
			BYTE b[2048] = { 0 };
			ZeroMemory(b, sizeof(b));
			DWORD sz = sizeof(b);
			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_LocationInfo, &type, b, sz, &sz, 0))
			{
				//Port_#0003.Hub_#0005
				int port, hub;
				_stscanf_s((TCHAR*)b, _T("Port_#%04d.Hub_#%04d"), &port, &hub);
				if (port == nPort)
				{
					bFind = TRUE;
					logIt(_T("find devices."));
					ZeroMemory(b, sizeof(b));
					if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_ProblemCode, &type, b, sz, &sz, 0))
					{
						//logIt(_T("Property: #%d\n"), i + 1);
						//dump(DEVPKEY_Device_ProblemCode, type, b, sz);
						if (type != DEVPROP_TYPE_UINT32)
						{
							logIt(_T("Error info. get DEVPKEY_Device_ProblemCode;"));
						}
						ret = *((DWORD*)b);
					}
					else
					{
						ret = GetLastError();
						OOPSERR(ret);
					}

					if (ret == ERROR_SUCCESS)
					{
						ZeroMemory(b, sizeof(b));
						sz = sizeof(b);
						if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_DevNodeStatus, &type, b, sz, &sz, 0))
						{
							//logIt(_T("Property: #%d\n"), i + 1);
							//dump(DEVPKEY_Device_ProblemCode, type, b, sz);
							if (type != DEVPROP_TYPE_UINT32)
							{
								logIt(_T("Error info. get DEVPKEY_Device_ProblemCode;"));
							}
							DWORD nodestatus = *((DWORD*)b);
							if ((nodestatus & (DN_DRIVER_LOADED | DN_STARTED)) == (DN_DRIVER_LOADED | DN_STARTED))
							{
								ret = ERROR_SUCCESS;
							}
							else
							{
								ret = nodestatus;
								OOPSERR(ret);
							}
						}
						else
						{
							ret = GetLastError();
							OOPSERR(ret);
						}
					}
				}
				else
				{
					ret = ERROR_DEVICE_REMOVED;
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}
	return ret;
}

int GetDeviceSymblInstanceIdEx(TCHAR *device_iid, LPWSTR lpReturnedString, DWORD nSize, GUID *guid)
{
	// Open an enumeration handle so we can locate all devices of our
	// own class
	int ret = ERROR_UNKNOWN_FEATURE;

	HDEVINFO info = SetupDiGetClassDevs(guid, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);
	if (info == INVALID_HANDLE_VALUE)
		return GetLastError();

	// Enumerate all devices of our class. For each one, create a
	// CDeviceEntryList object. Then determine the friendly name of the
	// device by reading the registry.

	SP_DEVICE_INTERFACE_DATA ifdata;
	ifdata.cbSize = sizeof(ifdata);
	DWORD devindex;
	for (devindex = 0; SetupDiEnumDeviceInterfaces(info, NULL, guid, devindex, &ifdata); ++devindex)
	{						// for each device

		// Determine the symbolic link name for this device instance. Since
		// this is variable in length, make an initial call to determine
		// the required length.

		DWORD needed;
		SP_DEVINFO_DATA did = { sizeof(SP_DEVINFO_DATA) };

		SetupDiGetDeviceInterfaceDetail(info, &ifdata, NULL, 0, &needed, &did);

		TCHAR sInstanceId[MAX_PATH] = { 0 };
		if (SetupDiGetDeviceInstanceId(info, &did, sInstanceId, MAX_PATH, NULL))
		{
			if (_tcsncicmp(sInstanceId, device_iid, _tcslen(device_iid)) == 0)
			{
				TCHAR  DetailDataBuffer[1024] = { 0 };
				PSP_DEVICE_INTERFACE_DETAIL_DATA DetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)DetailDataBuffer;
				DetailData->cbSize = sizeof SP_INTERFACE_DEVICE_DETAIL_DATA;

				if (SetupDiGetDeviceInterfaceDetail(info, &ifdata, DetailData, needed, NULL, NULL))
				{						// can't get detail info
					_stprintf_s(lpReturnedString, nSize, DetailData->DevicePath);
					break;
				}
				else// can't get detail info
				{
					ret = GetLastError();
					OOPSERR(ret);
				}

			}
		}

	}						// for each device

	SetupDiDestroyDeviceInfoList(info);

	return ret;
}							// CDeviceList::Initialize

int GetDeviceSymblInstanceId(TCHAR *device_iid, LPWSTR lpReturnedString, DWORD nSize)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	logIt(_T("GetDeviceSymblInstanceId is deprecated, It is Only Support USB Devices.\n"));
	if (device_iid == NULL || _tcslen(device_iid) == 0)
	{
		logIt(_T("GetDeviceSymblInstanceId error"));
		return ERROR_INVALID_PARAMETER;
	}

	HDEVINFO hDevHandle;
	SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
	deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

	int nBufferSize = 0;

	SP_DEVINFO_DATA devInfoData;
	devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	hDevHandle = SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB_DEVICE, NULL, NULL,
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

	if (hDevHandle != INVALID_HANDLE_VALUE)
	{
		TCHAR DetailDataBuffer[1024] = { 0 };
		DWORD required = 1024;
		for (DWORD MemberIndex = 0; SetupDiEnumDeviceInfo(hDevHandle, MemberIndex, &devInfoData); MemberIndex++)
		{
			if (!SetupDiEnumDeviceInterfaces(hDevHandle, 0, &GUID_DEVINTERFACE_USB_DEVICE, MemberIndex, &deviceInterfaceData))
			{
				SetupDiDestroyDeviceInfoList(hDevHandle);
				ret = GetLastError();
				OOPSERR(ret);
			}

			TCHAR DeviceInstanceID[MAX_PATH];
			memset(DeviceInstanceID, 0, MAX_PATH);

			if (SetupDiGetDeviceInstanceId(hDevHandle, &devInfoData, DeviceInstanceID, MAX_PATH, 0))
			{
				if (_tcsncicmp(DeviceInstanceID, device_iid, _tcslen(device_iid)) == 0)
				{
					PSP_DEVICE_INTERFACE_DETAIL_DATA DetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)DetailDataBuffer;
					DetailData->cbSize = sizeof SP_INTERFACE_DEVICE_DETAIL_DATA;
					nBufferSize = required;
					if (SetupDiGetDeviceInterfaceDetail(hDevHandle, &deviceInterfaceData, DetailData, nBufferSize, NULL, NULL))
					{
						_stprintf_s(lpReturnedString, nSize, DetailData->DevicePath);
						break;
					}
					else
					{
						ret = GetLastError();
						OOPSERR(ret);
					}
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		};
		SetupDiDestroyDeviceInfoList(hDevHandle);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}


	//HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	//if (hDevInfo != INVALID_HANDLE_VALUE)
	//{
	//	SP_DEVINFO_DATA devInfoData = { 0 };
	//	devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	//	if (SetupDiOpenDeviceInfo(hDevInfo, device_iid, NULL, 0, &devInfoData))
	//	{
	//		HDEVINFO hInterface = SetupDiGetClassDevs(&devInfoData.ClassGuid, NULL, NULL, DIGCF_DEVICEINTERFACE);
	//		if (hInterface != INVALID_HANDLE_VALUE)
	//		{
	//			for (DWORD idx2 = 0;; idx2++)
	//			{

	//				SP_DEVICE_INTERFACE_DATA data;
	//				data.cbSize = sizeof(data);

	//				if (!SetupDiEnumDeviceInterfaces(hInterface, NULL, &devInfoData.ClassGuid, idx2, &data))
	//				{
	//					if (GetLastError() != ERROR_NO_MORE_ITEMS)
	//						logIt(_T("SetupDiEnumDeviceInterfaces = %d\n"), GetLastError());
	//					break;
	//				}

	//				DWORD DetailDataSize = 1024;
	//				TCHAR  DetailDataBuffer[1024] = {0};
	//				PSP_DEVICE_INTERFACE_DETAIL_DATA DetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)DetailDataBuffer;
	//				DetailData->cbSize = sizeof SP_INTERFACE_DEVICE_DETAIL_DATA;

	//				SP_DEVINFO_DATA                 DeviceInfoData = { 0 };
	//				DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	//				
	//				if (SetupDiGetDeviceInterfaceDetail(hInterface, &data, DetailData,
	//					DetailDataSize, NULL, &DeviceInfoData))
	//				{
	//					TCHAR sInstanceId[MAX_PATH] = { 0 };
	//					if (SetupDiGetDeviceInstanceId(hInterface, &DeviceInfoData, sInstanceId, MAX_PATH, NULL))
	//					{
	//						if (_tcsncicmp(sInstanceId, device_iid, _tcslen(device_iid)) == 0)
	//						{
	//							_stprintf_s(lpReturnedString, nSize, DetailData->DevicePath);
	//							break;
	//						}
	//					}
	//					logIt(_T("\tDevicePath = %s\n"), DetailData->DevicePath);
	//				}
	//				else
	//				{
	//					logIt(_T("SetupDiGetDeviceInterfaceDetail = %d\n"), GetLastError());
	//				}
	//			}
	//			SetupDiDestroyDeviceInfoList(hInterface);
	//		}
	//		else
	//		{
	//			ret = GetLastError();
	//			OOPSERR(ret);
	//		}
	//	}
	//	else
	//	{
	//		ret = GetLastError();
	//		OOPSERR(ret);
	//	}
	//}
	//else
	//{
	//	ret = GetLastError();
	//	OOPSERR(ret);
	//}

	return ret;

}

int GetDeviceInstanceId(TCHAR *sName, int nPort, LPWSTR lpReturnString, DWORD dwLen)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0 || nPort == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), sName);
	}

	BOOL bFind = FALSE;
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_Children, &type, b, 2048, &sz, 0))
				{
					//logIt(_T("Property: #%d\n"), i + 1);
					//dump(p, type, b, sz);
					if (type == DEVPROP_TYPE_STRING_LIST)
					{
						TCHAR *instanceid = (TCHAR *)b;
						DWORD dwoffset = 0;
						while (instanceid != NULL && _tcslen(instanceid)>0)
						{
							logIt(instanceid);

							ret = GetDeviceStatusByPort(instanceid, nPort, bFind);
							if (bFind)
							{
								if (lpReturnString == NULL || dwLen < _tcslen(instanceid) + 1)
								{
									ret = ERROR_INSUFFICIENT_BUFFER;
								}
								else
								{
									_tcscpy_s(lpReturnString, dwLen, instanceid);
								}
								break;
							}
							dwoffset += _tcslen(instanceid) + 1;
							instanceid = (TCHAR *)b + dwoffset;
						}
					}
					if (!bFind)
					{
						ret = ERROR_NO_MORE_ITEMS;
						OOPSERR(ret);
					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}

	return ret;
}

int GetHubPortIndx(TCHAR *sName, int &nPort)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), sName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_InstanceId, &type, b, 2048, &sz, 0))
				{
					STRING sInstanceid = (TCHAR *)b;
					ZeroMemory(b, sizeof(b));
					if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_Service, &type, b, 2048, &sz, 0))
					{
						TCHAR *sService = (TCHAR *)b;
						TCHAR sPath[1024] = { 0 };
						_stprintf_s(sPath, _T("SYSTEM\\CurrentControlSet\\services\\%s\\Enum"), sService);
						HKEY root;
						ret = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sPath, 0, KEY_READ | KEY_WOW64_32KEY, &root);
						if (ret == ERROR_SUCCESS)
						{
#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383
							TCHAR    achKey[MAX_KEY_LENGTH];
							DWORD dwSize = MAX_KEY_LENGTH;
							DWORD    cValues;
							ret = RegQueryInfoKey(root, NULL, NULL, NULL, NULL, NULL, NULL, &cValues, NULL, NULL, NULL, NULL);
							if (ret == ERROR_SUCCESS)
							{
								for (int i = 0, ret = ERROR_SUCCESS; i<cValues; i++)
								{
									dwSize = MAX_VALUE_NAME;
									achKey[0] = '\0';
									DWORD nType = 0;
									ZeroMemory(b, sizeof(b));
									sz = sizeof(b);
									ret = RegEnumValue(root, i,
										achKey,
										&dwSize,
										NULL,
										&nType,
										b,
										&sz);

									if (ret == ERROR_SUCCESS && nType == REG_SZ)
									{
										logIt(TEXT("%s == %s\n"), achKey, (TCHAR *)b);

										if (_tcsncicmp((TCHAR *)b, sInstanceid.c_str(), sInstanceid.length()) == 0)
										{
											nPort = _tstoi(achKey);
											break;
										}
									}
								}
							}
							else
							{
								ret = GetLastError();
								OOPSERR(ret);
							}
							//ret = RegQueryValueEx(root, _TEXT("InstallPath"), NULL, NULL, data1, &size);
						}
						else
						{
							ret = GetLastError();
							OOPSERR(ret);
						}
					}
					else
					{
						ret = GetLastError();
						OOPSERR(ret);
					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}

	return ret;

}

int GetDeviceStatus(TCHAR *sName, int nPort)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0 || nPort == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), sName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_Children, &type, b, 2048, &sz, 0))
				{
					//logIt(_T("Property: #%d\n"), i + 1);
					//dump(p, type, b, sz);
					if (type == DEVPROP_TYPE_STRING_LIST)
					{
						TCHAR *instanceid = (TCHAR *)b;
						DWORD dwoffset = 0;
						BOOL bFind = FALSE;
						while (instanceid != NULL && _tcslen(instanceid)>0)
						{
							logIt(instanceid); 
							
							ret = GetDeviceStatusByPort(instanceid, nPort, bFind);
							if (bFind)
							{
								break;
							}
							dwoffset += _tcslen(instanceid) + 1;
							instanceid = (TCHAR *)b + dwoffset;
						}

					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}

	return ret;
}

int GetDeviceCommonSymbl(TCHAR *sName, int nPort, PDEVPROPKEY pdevkey, PBYTE pdata, DWORD &nLen, DWORD &nType)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0 || nPort == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), sName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			BYTE b[4096];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, pdevkey, &nType, b, 2048, &sz, 0))
				{
					//logIt(_T("Property: #%d\n"), i + 1);
					//dump(p, type, b, sz);
					if (sz <= nLen){
						memcpy_s(pdata, nLen, b, sz);
						ret = ERROR_SUCCESS;
					}
					else
						ret = ERROR_INSUFFICIENT_BUFFER;
					nLen = sz;
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}

	return ret;
}

int GetDeviceCommonHW(TCHAR *device_iid, PDEVPROPKEY pdevkey, PBYTE pdata, DWORD &nLen, DWORD &nType)
{
	int ret = ERROR_INVALID_PARAMETER;
	if (device_iid == NULL || _tcslen(device_iid) == 0)
		return ret;
	if (pdata == NULL || nLen == 0) return ERROR_INSUFFICIENT_BUFFER;
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{

		SP_DEVINFO_DATA devInfoData;
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, device_iid, NULL, 0, &devInfoData))
		{
			BYTE b[4096] = { 0 };
			ZeroMemory(b, sizeof(b));
			DWORD sz = sizeof(b);
			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, pdevkey, &nType, b, sz, &sz, 0))
			{
				if (sz <= nLen){
					memcpy_s(pdata, nLen, b, sz);
					ret = ERROR_SUCCESS;
				}
				else
					ret = ERROR_INSUFFICIENT_BUFFER;
				nLen = sz;
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}
	return ret;
}

int GetParentHubSymb(STRING sHWid, STRING &symbl)
{
	int ret = 0;
	HDEVINFO hDevHandle;
	SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
	DWORD required = 0;
	deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

	int nBufferSize = 0;

	SP_DEVINFO_DATA devInfoData = { 0 };
	devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	DWORD MemberIndex = -1;
	BOOL  Result;

	hDevHandle = SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB_HUB, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDevHandle != INVALID_HANDLE_VALUE)
	{
		TCHAR *buffer = NULL;
		PSP_DEVICE_INTERFACE_DETAIL_DATA devicedetailData;
		do
		{
			MemberIndex++;
			Result = SetupDiEnumDeviceInfo(hDevHandle, MemberIndex, &devInfoData);
			if (Result)
			{
				TCHAR szDescription[MAX_PATH];
				memset(szDescription, 0, MAX_PATH);
				if (SetupDiGetDeviceInstanceId(hDevHandle, &devInfoData, szDescription, MAX_PATH, 0))
				{
					if (_tcsncicmp(sHWid.c_str(), szDescription, sHWid.size()) == 0)
					{
						Result = SetupDiEnumDeviceInterfaces(hDevHandle, 0, &GUID_DEVINTERFACE_USB_HUB, MemberIndex, &deviceInterfaceData);
						if (Result)
						{
							SetupDiGetDeviceInterfaceDetail(hDevHandle, &deviceInterfaceData, NULL, 0, &required, NULL);
							if (GetLastError() == ERROR_INSUFFICIENT_BUFFER && required > 0)
							{
								buffer = new TCHAR[required];
								if (buffer != NULL)
								{
									devicedetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)buffer;
									devicedetailData->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

									if (SetupDiGetDeviceInterfaceDetail(hDevHandle, &deviceInterfaceData, devicedetailData, required, &required, NULL))
									{
										symbl = devicedetailData->DevicePath;
										ret = ERROR_SUCCESS;
									}
									else
									{
										ret = GetLastError();
										OOPSERR(ret);
									}
									delete[]buffer;
									buffer = NULL;
								}
								else
								{
									logIt(_T("Alloc memery failed.\n"));
									ret = ERROR_NOT_ENOUGH_MEMORY;
									OOPSERR(ret);
								}
							}
							else
							{
								ret = GetLastError();
								OOPSERR(ret);
							}
						}
						else
						{
							ret = GetLastError();
							OOPSERR(ret);
						}

						break;
					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		} while (ret != ERROR_NO_MORE_ITEMS);
	}

	return ret;
}

int FindCalibrationIndex(STRING sCalibration, int nPort, STRING symbl)
{
	int ret = 0;
	if (nPort == 0 || symbl.size() == 0)
		return -ERROR_INVALID_PARAMETER;
#define MAXBUFSIZE		32767
	int nBufferSize = MAXBUFSIZE;
	TCHAR lpszReturnBuffer[MAXBUFSIZE] = { 0 };
	GetPrivateProfileSection(_T("label"), lpszReturnBuffer, nBufferSize, sCalibration.c_str());
	DWORD dwOffset = 0;
	for (; 0 != _tcslen(lpszReturnBuffer + dwOffset);)
	{
		//GetPrivateProfileString(_T("label"), lpszReturnBuffer + dwOffset, _T(""), lpszValue, nBufferSize, sCalibration.c_str());
		logIt(lpszReturnBuffer + dwOffset);
		TCHAR lpszValue[1024] = { 0 };
		_stprintf_s(lpszValue, _T("%d@%s"), nPort, symbl.c_str() + _tcslen(_T("\\\\?\\")));//4 
		_tcslwr_s(lpszValue, _tcslen(lpszValue)+1);
		_tcslwr_s(lpszReturnBuffer + dwOffset, _tcslen(lpszReturnBuffer + dwOffset)+1);
		STRING s(lpszReturnBuffer + dwOffset);
		if (s.find(lpszValue) != STRING::npos)
		{
			size_t sindex = s.find(_T("="));
			if (sindex != STRING::npos)
			{
				s = s.substr(0, sindex);
				ret = _tstoi(s.c_str());
			}
			break;
		}
		dwOffset += _tcslen(lpszReturnBuffer + dwOffset) + 1;
	}

	return ret;
}

void Tokenize(const STRING& str,
	std::vector<STRING>& tokens,
	const STRING& delimiters = _T(" "))
{
	// Skip delimiters at beginning.
	STRING::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	STRING::size_type pos = str.find_first_of(delimiters, lastPos);

	while (STRING::npos != pos || STRING::npos != lastPos)
	{
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}
}

int Split(const STRING& str, std::vector<STRING>& ret_, STRING sep = _T("\\"))
{
	if (str.empty())
	{
		return 0;
	}

	STRING tmp;
	STRING::size_type pos_begin = str.find_first_not_of(sep);
	STRING::size_type comma_pos = 0;

	while (pos_begin != STRING::npos)
	{
		comma_pos = str.find(sep, pos_begin);
		if (comma_pos != STRING::npos)
		{
			tmp = str.substr(pos_begin, comma_pos - pos_begin);
			pos_begin = comma_pos + sep.length();
		}
		else
		{
			tmp = str.substr(pos_begin);
			pos_begin = comma_pos;
		}

		if (!tmp.empty())
		{
			ret_.push_back(tmp);
			tmp.clear();
		}
	}
	return 0;
}


STRING Replace(const STRING& str, const STRING& src, const STRING& dest)
{
	STRING ret;

	size_t pos_begin = 0;
	size_t pos = str.find(src);
	while (pos != STRING::npos)
	{
		ret.append(str.data() + pos_begin, pos - pos_begin);
		ret += dest;
		pos_begin = pos + 1;
		pos = str.find(src, pos_begin);
	}
	if (pos_begin < str.length())
	{
		ret.append(str.begin() + pos_begin, str.end());
	}
	return ret;
}


int GetDeviceLabel(STRING sHWid, STRING sCalibration)
{
	int ret = 0;

	int port = 0, hub = 0;
	if (sHWid.empty() || !PathFileExists(sCalibration.c_str()))
		return ret;
	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{

		SP_DEVINFO_DATA devInfoData;
		devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		if (SetupDiOpenDeviceInfo(hDevInfo, sHWid.c_str(), NULL, 0, &devInfoData))
		{
			DEVPROPTYPE type;
			BYTE b[2048] = { 0 };
			ZeroMemory(b, sizeof(b));
			DWORD sz = sizeof(b);

			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_LocationInfo, &type, b, sz, &sz, 0))
			{
				//logIt(_T("Property: #%d\n"), i + 1);
				//dump(DEVPKEY_Device_ProblemCode, type, b, sz);
				if (type != DEVPROP_TYPE_STRING)
				{
					logIt(_T("Error info. get DEVPKEY_Device_LocationInfo;"));
				}
				if (_stscanf_s((TCHAR*)b, _T("Port_#%04d.Hub_#%04d"), &port, &hub) != 2)
				{
					logIt(_T("It is USB Device Parent Driver."));
					SetupDiDestroyDeviceInfoList(hDevInfo);
					return -ERROR_INVALID_PARAMETER;
				}
			}
			else
			{
				ret = 0-GetLastError();
				OOPSERR(ret);
			}
			
			ZeroMemory(b, sizeof(b));
			sz = sizeof(b);
			
			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_Parent, &type, b, sz, &sz, 0))
			{
				//logIt(_T("Property: #%d\n"), i + 1);
				//dump(DEVPKEY_Device_ProblemCode, type, b, sz);
				if (type != DEVPROP_TYPE_STRING)
				{
					logIt(_T("Error info. get DEVPKEY_Device_Parent;"));
				}
				STRING sParentID = (TCHAR *)b;
				STRING sSymblink;// = Replace(sParentID, _T("\\"), _T("#"));
				//sSymblink = _T("\\\\?\\") + sSymblink + _T("#{f18a0e88-c30c-11d0-8815-00a0c906bed8}");
				//DEVINST diParent = 0;

				//if (CM_Get_Parent(&diParent, devInfoData.DevInst, 0) == CR_SUCCESS)
				//{
				//	if (CM_Get_DevNode_Property(diParent, &) == CR_SUCCESS)
				//	{

				//	}
				//}
				

				if (GetParentHubSymb(sParentID, sSymblink) == ERROR_SUCCESS)
				{
					ret = FindCalibrationIndex(sCalibration, port, sSymblink);
				}
			}
			else
			{
				ret = 0-GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = 0-GetLastError();
			OOPSERR(ret);
		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = 0-GetLastError();
		OOPSERR(ret);
	}

	return ret;
}

int  GetChildPropertyHubPort(
	_In_	 int nlabel,
	_In_     LPCWSTR sHubName,
	_In_	 int nPort,
	_In_	 PDEVPROPKEY pdevkey,
	_Out_    PBYTE nRet,
	_Inout_  int &nLen,
	_Out_    DWORD &nType
	)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	ENTER_FUNCTION();
	if (sHubName == NULL || _tcslen(sHubName) == 0 || nPort == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sHubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sHubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sHubName);
	}
	else
	{
		_stprintf_s(symblName, _T("\\\\?\\%s"), sHubName);
	}

	TCHAR sDriverKey[MAX_PATH] = { 0 };

	SECURITY_ATTRIBUTES SA = { 0 };
	SA.nLength = sizeof(SECURITY_ATTRIBUTES);
	HANDLE HubHandle = CreateFile(symblName, GENERIC_WRITE|GENERIC_READ, FILE_SHARE_WRITE|FILE_SHARE_READ, &SA, OPEN_EXISTING, 0, NULL);
	if (HubHandle != INVALID_HANDLE_VALUE)
	{
		DWORD BytesReturned=0;
		USB_NODE_CONNECTION_INFORMATION  Node_ConnInfo;
		Node_ConnInfo.ConnectionIndex = nPort;
		BOOL Success = DeviceIoControl(HubHandle, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION,
			&Node_ConnInfo, sizeof(Node_ConnInfo), &Node_ConnInfo, sizeof(Node_ConnInfo), &BytesReturned, NULL);
		if (Success && Node_ConnInfo.ConnectionStatus == DeviceConnected)
		{
			struct USB_NODE_CONNECTION_NAME {
				ULONG ConnectionIndex;  /* INPUT */
				ULONG ActualLength;     /* OUTPUT */
				/* unicode name for the devnode. */
				WCHAR DriverKeyName[MAX_PATH];      /* OUTPUT */
			} Node_DriverKeyName;
			Node_DriverKeyName.ConnectionIndex = nPort;
			Success = DeviceIoControl(HubHandle, IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME,
				&Node_DriverKeyName, sizeof(struct USB_NODE_CONNECTION_NAME),
				&Node_DriverKeyName, sizeof(struct USB_NODE_CONNECTION_NAME), &BytesReturned, NULL);

			if (Success)
			{
				_stprintf_s(sDriverKey, _T("%s"), Node_DriverKeyName.DriverKeyName);
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			logIt(_T("Get Node Connection Info ret=%d, connectionstatus=%d\n"), ret, Node_ConnInfo.ConnectionStatus);
			ret = ERROR_NO_DEVICE_SELECTED;
		}
		CloseHandle(HubHandle);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
		return ret;
	}
	if (_tcslen(sDriverKey) == 0)
	{
		logIt(_T("can not get Driverkey.\n"));
		return ret;
	}
	logIt(_T("found driverkey = %s"), sDriverKey);
	BYTE data[4096] = { 0 };
	DWORD nLenN = sizeof(data);
	DWORD nType1 = 0;
	TCHAR connectDevInstanceID[MAX_PATH] = { 0 };
	BOOL bFind = false;
	ret = GetDeviceCommonSymbl(symblName, nPort, (PDEVPROPKEY)&DEVPKEY_Device_Children, data, nLenN, nType1);
	if (ret == ERROR_SUCCESS)
	{
		TCHAR *instanceid = (TCHAR *)data;
		DWORD dwoffset = 0;
		while (instanceid != NULL && _tcslen(instanceid)>0)
		{
			logIt(instanceid);

			ret = GetDeviceDriverkey(instanceid, sDriverKey, bFind);
			if (bFind)
			{
				logIt(_T("InstanceID = %s"), instanceid);
				_tcscpy_s(connectDevInstanceID, MAX_PATH, instanceid);
				break;
			}
			dwoffset += _tcslen(instanceid) + 1;
			instanceid = (TCHAR *)data + dwoffset;
		}

		if (_tcslen(connectDevInstanceID) == 0)
		{
			ret = ERROR_NOT_FOUND;
		}
		else
		{
			nLenN = nLen;
			ret = GetDeviceCommonHW(connectDevInstanceID, pdevkey, nRet, nLenN, nType);
			nLen = nLenN;
		}
	}

	return ret;
}

int  GetDeviceStatusHW(int nlabel, LPCTSTR sHwid)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	g_args.sHWid = sHwid;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetDeviceStatus((TCHAR *)sHwid);
}

int  GetDeviceStatusSymbl(int nlabel, LPCTSTR symbl, int nPort)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	g_args.sName = symbl;
	g_args.nPort = nPort;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetDeviceStatus((TCHAR *)symbl, nPort);
}

int  GetDeviceLabelPort(int nlabel, LPCTSTR sHwid, LPCTSTR sFileName)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	g_args.sHWid = sHwid;
	g_args.sCalibration = sFileName;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetDeviceLabel(sHwid, sFileName);
}

int  GetDeviceInstanceIdFromSymbl(
	_In_	 int nlabel,
	_In_     LPCWSTR symbl,
	_In_	 int nPort,
	_Out_writes_to_opt_(nSize, return +1) LPWSTR lpReturnedString,
	_In_     DWORD nSize
	)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	g_args.sName = symbl;
	g_args.nPort = nPort;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetDeviceInstanceId((TCHAR *)symbl, nPort, lpReturnedString, nSize);
}

int  GetDeviceSymblFromInstanceId(
	_In_	 int nlabel,
	_In_     LPCWSTR sInstanceId,
	_Out_writes_to_opt_(nSize, return +1) LPWSTR lpReturnedString,
	_In_     DWORD nSize
	)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetDeviceSymblInstanceId((TCHAR *)sInstanceId, lpReturnedString, nSize);
}

int  GetDeviceSymblFromInstanceIdEx(
	_In_	 int nlabel,
	_In_     LPCWSTR sInstanceId,
	_Out_writes_to_opt_(nSize, return +1) LPWSTR lpReturnedString,
	_In_     DWORD nSize,
	_In_     GUID interfaceGuid
	)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetDeviceSymblInstanceIdEx((TCHAR *)sInstanceId, lpReturnedString, nSize, &interfaceGuid);
}

int  dllmain(int argc, _TCHAR* argv[])
{
	int ret = ERROR_INVALID_PARAMETER;
	TPARAMETERS _args;
	if (NO_ERROR != parseCommandLine(argc, argv, _args))
	{
		usage();
		return ret;
	}

	if ((_args.nPort == 0 || _args.sName.empty()) && (_args.sHWid.empty()))
	{
		usage();
		return ret;
	}

	if (!_args.sPropKey.empty())
	{
		DEVPROPKEY devpropkey;
		std::vector<STRING> __ret;
		if (Split(_args.sPropKey, __ret) == 2)
		{
			//devpropkey.fmtid = __ret.at(0)
			CLSIDFromString(__ret.at(0).c_str(), &devpropkey.fmtid);
			devpropkey.pid = _tstoi(__ret.at(1).c_str());
			BYTE pData[4096] = { 0 };
			DWORD nLen = sizeof(pData);
			DWORD nType = 0;
			if (!_args.sHWid.empty())
			{
				ret = GetDeviceCommonHW((TCHAR *)_args.sHWid.c_str(), &devpropkey, pData, nLen, nType);
			}
			else
			{
				ret = GetDeviceCommonSymbl((TCHAR *)_args.sName.c_str(), _args.nPort,  &devpropkey, pData, nLen, nType);
			}
			if (ret == ERROR_SUCCESS)
			{
				dump(&devpropkey, nType, pData, nLen);
			}
		}
		
	}

	else if (!_args.sHWid.empty())
	{
		if (_args.sCalibration.empty())
		{
			ret = GetDeviceStatus((TCHAR *)_args.sHWid.c_str());
		}
		else
		{
			ret = GetDeviceLabel(_args.sHWid, _args.sCalibration);
		}
	}
	else
	{
		ret = GetDeviceStatus((TCHAR *)_args.sName.c_str(), _args.nPort);
	}

	return ret;
}

typedef NTSTATUS(NTAPI *_NtQuerySemaphore)(
	HANDLE SemaphoreHandle,
	DWORD SemaphoreInformationClass, /* Would be SEMAPHORE_INFORMATION_CLASS */
	PVOID SemaphoreInformation,      /* but this is to much to dump here     */
	ULONG SemaphoreInformationLength,
	PULONG ReturnLength OPTIONAL
	);

typedef struct _SEMAPHORE_BASIC_INFORMATION {
	ULONG CurrentCount;
	ULONG MaximumCount;
} SEMAPHORE_BASIC_INFORMATION;


int  GetSemaphoreCurCnt(LPTSTR sSemaphoreName)
{
	_NtQuerySemaphore NtQuerySemaphore;
	HANDLE Semaphore;
	SEMAPHORE_BASIC_INFORMATION BasicInfo;
	NTSTATUS Status;
	int ret = -1;

	Semaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, FALSE, sSemaphoreName);
	if (Semaphore != NULL)
	{
		char *funName = "NtQuerySemaphore";
		HMODULE g_ntdll = GetModuleHandle(_T("ntdll.dll"));
		NtQuerySemaphore = (_NtQuerySemaphore)GetProcAddress(g_ntdll, funName);

		if (NtQuerySemaphore)
		{

			Status = NtQuerySemaphore(Semaphore, 0 /*SemaphoreBasicInformation*/,
				&BasicInfo, sizeof(SEMAPHORE_BASIC_INFORMATION), NULL);

			if (Status == ERROR_SUCCESS)
			{
				//logIt(_T("CurrentCount: %lu"), BasicInfo.CurrentCount);
				ret = ((int)(BasicInfo.MaximumCount<<16) | (int)BasicInfo.CurrentCount);
			}
			else
				ret = 0 - GetLastError();

		}
		else
			ret = 0 - GetLastError();

		CloseHandle(Semaphore);
	}
	else
		ret = 0 - GetLastError();

	return ret;
}

int  GetHubPortIndex(int nlabel, LPCTSTR sHubName)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	g_args.sName = sHubName;
	LeaveCriticalSection(&g_args.csLockglobe);
	int nPort = -1;
	int ret = GetHubPortIndx((TCHAR *)sHubName, nPort);
	if (ret == ERROR_SUCCESS)
	{
		return nPort;
	}
	else
	{
		return 0 - ret;
	}
}

int  GetCommonSymbl(int nlabel, LPCTSTR sHubName, int nPort, PDEVPROPKEY pdevkey, PBYTE pData, DWORD &nLen, DWORD &nType)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	g_args.sName = sHubName;
	g_args.nPort = nPort;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetDeviceCommonSymbl((TCHAR *)sHubName, nPort, pdevkey, pData, nLen, nType);
}

int  GetCommonHW(int nlabel, LPCTSTR sHwid, PDEVPROPKEY pdevkey, PBYTE pData, DWORD &nLen, DWORD &nType)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	g_args.sHWid = sHwid;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetDeviceCommonHW((TCHAR *)sHwid, pdevkey, pData, nLen, nType);
}

int  GetChildPropertyFromHubPort(
	_In_	 int nlabel,
	_In_     LPCWSTR sHubName,
	_In_	 int nPort,
	_In_	 PDEVPROPKEY pdevkey,
	_Out_    PBYTE nRet,
	_Inout_  int &nLen,
	_Out_    DWORD &nType
	)
{
	EnterCriticalSection(&g_args.csLockglobe);
	g_args.label = nlabel;
	g_args.sName = sHubName;
	g_args.nPort = nPort;
	LeaveCriticalSection(&g_args.csLockglobe);
	return GetChildPropertyHubPort(
		nlabel,
		sHubName,
		nPort,
		pdevkey,
		nRet,
		nLen,
		nType
		);
}

int  CheckHwid(int nlabel, LPCTSTR sHwid, LPCTSTR sInfname)
{
	int ret = ERROR_NOT_FOUND;
	_logIt(_T("[label_%d] CheckHwid: ++ sHwid=%s, inf=%d\n"), nlabel, sHwid, sInfname);
	if (checkHwidExist(nlabel, sHwid, sInfname))
		ret = NO_ERROR;
	_logIt(_T("[label_%d] CheckHwid: -- ret=%d\n"), nlabel, ret);
	return ret;
}

int  GetDevicePropertiesByDeviceInstanceId(int nlabel, LPCTSTR sHwid, _DeviceProperty** pdata, int dataCount)
{
	int ret = ERROR_INVALID_PARAMETER;
	_logIt(_T("[label_%d] GetDevicePropertiesByDeviceInstanceId: ++ sHwid=%s, property=%d\n"), nlabel, sHwid, dataCount);
	if (pdata!=NULL)
		ret = GetDevicePropertiesByDeviceInstanceIdEx(nlabel, sHwid, pdata, dataCount);
	_logIt(_T("[label_%d] GetDevicePropertiesByDeviceInstanceId: -- ret=%d\n"), nlabel, ret);
	return ret;
}

int  GetDevicePropertiesByDeviceSerialNumber(int nlabel, LPCTSTR sSN, _Out_writes_to_opt_(nSize, return +1) LPWSTR lpHubName,
	_Inout_  DWORD &nSizePort,//in lpHubName Buffer Length, Out is Hub port
	_DeviceProperty** pdata, int dataCount)
{
	int ret = ERROR_INVALID_PARAMETER;
	_logIt(_T("[label_%d] GetDevicePropertiesByDeviceSerialNumber: ++ sHwid=%s, property=%d\n"), nlabel, sSN, dataCount);
	if (pdata != NULL)
		ret = GetDevicePropertiesByDeviceSerialNumberEx(nlabel, sSN, lpHubName, nSizePort, pdata, dataCount);
	_logIt(_T("[label_%d] GetDevicePropertiesByDeviceSerialNumber: -- ret=%d\n"), nlabel, ret);
	return ret;

}

int _tmain(int argc, _TCHAR* argv[])
{
	//GetSemaphoreCurCnt(_T("IRESTORE_SEM"));
	//FindCalibrationIndex(_T("D:\\temp\\calibration.ini"), 7, _T("\\\\?\\USB#vid_0424&pid_2517#7&14d2eb4b&0&2#{f18a0e88-c30c-11d0-8815-00a0c906bed8}"));
	//return test2();
	//TCHAR retas[1024];
	//GetDeviceInstanceId(_T("USB#VID_8087&PID_0024#5&e3a39ee&0&1#{f18a0e88-c30c-11d0-8815-00a0c906bed8}"), 3, retas, 1024);
	//int nPort = -1;
	//GetHubPortIndx(_T("USB#VID_8087&PID_0024#5&e3a39ee&0&1#{f18a0e88-c30c-11d0-8815-00a0c906bed8}"), nPort);

	//GetDeviceStatus_Test(_T("USB\\VID_05AC&PID_12A0\\0888a62393db94b72fe721bb48e7c6d9e50d5fef"));

	int ret = ERROR_INVALID_PARAMETER;
	if (NO_ERROR != parseCommandLine(argc, argv, g_args))
	{
		usage();
		return ret;
	}
	if ((g_args.nPort == 0 || g_args.sName.empty()) && (g_args.sHWid.empty()))
	{
		usage();
		return ret;
	}

	if (!g_args.sHWid.empty())
	{
		if (g_args.sCalibration.empty())
		{
			ret = GetDeviceStatus((TCHAR *)g_args.sHWid.c_str());
		}
		else
		{
			ret = GetDeviceLabel(g_args.sHWid, g_args.sCalibration);
		}
	}
	else
	{
		ret = GetDeviceStatus((TCHAR *)g_args.sName.c_str(), g_args.nPort);
	}

	return ret;
}

